package main;

public class HWrand {

    public void rndm(){
        System.out.println((int)(Math.random() * 10));
    }

    public void rnd10(){
        for (int i = 0; i < 10; i++) {
            System.out.print((int)(Math.random() * 100) + " ");
        }
        System.out.println();
    }

    public void rnd10from0to10(){
        for (int i = 0; i < 10; i++) {
            System.out.print((int)(Math.random() * 10) + " ");
        }
        System.out.println();
    }

    public void rnd10from20to50(){
        for (int i = 0; i < 10; i++) {
            System.out.print((int)(Math.random() * 30 + 20) + " ");
        }
        System.out.println();
    }

    public void rnd10fromMinus10to10(){
        for (int i = 0; i < 10; i++) {
            System.out.print((int)(Math.random() * 20 - 10) + " ");
        }
        System.out.println();
    }

    public void rndrnd(){
        for (int i = 0; i < (int)(Math.random() * 12 + 3); i++) {
            System.out.print((int)(Math.random() * 45 - 10) + " ");
        }
        System.out.println();
    }
}
