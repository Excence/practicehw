package main;

public class Main {

    public static void main(String[] args) {

        HWrand hwrnd = new HWrand();

        System.out.print("Случайное число " ); hwrnd.rndm();
        System.out.print("Десять случайных чисел " ); hwrnd.rnd10();
        System.out.print("Десять случайных чисел в диапазоне от 0 до 10" ); hwrnd.rnd10from0to10();
        System.out.print("Десять случайных чисел в диапазоне от 20 до 50" ); hwrnd.rnd10from20to50();
        System.out.print("Десять случайных чисел в диапазоне от -10 до 10" ); hwrnd.rnd10fromMinus10to10();
        System.out.print("Случайное количество (в диапазоне от 3 до 15) случайных чисел, в диапазоне от -10 до 35 : " ); hwrnd.rndrnd();

        //--------------------------------------------------
        System.out.println("---------------------------------------------------------------------------------------------");
        //--------------------------------------------------

        HWmath hwmath = new HWmath();

        System.out.println("Дальность полета ядра равна "+hwmath.howitzer(0,35,1) + " м"); // радианы
        System.out.println("Дальность полета ядра равна "+hwmath.howitzer(1,76,45) + " м"); // градусы
        System.out.println("Расстояние между машинами " +hwmath.twoCars(65, 83, 64, 1.5) + " км");
        System.out.print("Точка (0,1;-0,1) ");hwmath.figure(0.1, -0.1);System.out.println(" заштрихованой фигуре");
        System.out.println("Значение выражения равно " + hwmath.avis(15));
    }
}
