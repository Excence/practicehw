package main;

public class HWmath {

    public double howitzer(int c, double v, double cor){

        double rez=0;

        switch (c) {
            case 0 : rez = (Math.pow(v , 2)*Math.sin(2*cor))/9.80665;
                break;
            case 1 : rez = (Math.pow(v , 2)*Math.sin(2*Math.toRadians(cor)))/9.80665;
                break;
        }
        return rez;
    }

    public double twoCars(double v1, double v2, double s, double t){
        double rez;

        rez = (v1+v2)*t+s;

        return rez;
    }

    public void figure(double x, double y){
        if (((x>=0) && (y>= -1) && (y<=2) && (y >= 1.5*x-1) && (y <= x)) || ((x<=0) && (y>=-x) && (y>= -1) && (y<=2) && (y >= -1.5*x-1))){
            System.out.print("принадлежит");
        } else {System.out.print("не принадлежит");}
    }

    public double avis(double x){
        return (6*Math.log(Math.sqrt((Math.exp(x+1)+2*Math.exp(x)*Math.cos(x)))))/(Math.log(x-Math.exp(x=1)*Math.sin(x)))+Math.abs(Math.cos(x)/Math.exp(Math.sin(x)));
    }
}
